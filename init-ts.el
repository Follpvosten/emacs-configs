;;; .emacs -- My cool Emacs configuration for TypeScript development

;;; Commentary:
;; Important note: If you want to use this configuration, you need the Perl
;; program "csswatcher".  To install that, install Perl and run
;; `sudo cpan i CSS::Watcher`
;; A user-local installation sadly isn't possible, but everything should work
;; after that.

;;; Code:
;;; FlyCheck wants me to add this section.
;;; But this is actually...

;;; Generated stuff:
;;; Things that are added automagically, meaning not by me.
;;; ...but the next directives actually are.  Just nothing after that.
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (json-mode markdown-mode tide sass-mode ac-html-csswatcher company-web company sr-speedbar git-commit magit web-mode flycheck use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(condition-case nil
    (require 'use-package)
  (file-error
   (package-refresh-contents)
   (package-install 'use-package)
   (require 'use-package)
   )
  )

;; use-package calls
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode)
  )

(use-package web-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (defun my-web-mode-hook ()
    "web-mode customizing"
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-enable-auto-pairing t)
    )
  (add-hook 'web-mode-hook 'my-web-mode-hook)
  )

(use-package company
  :ensure t
  :config
  (global-company-mode)
  (setq company-tooltip-limit 15)
  (setq company-idle-delay 0)
  (setq company-echo-delay 0)
  (setq company-minimum-prefix-length 2)
  (setq company-require-match nil)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)
  (add-hook 'eshell-mode-hook '(lambda () (setq-local company-idle-delay nil)))

  ;; Company backends
  (use-package company-web
    :ensure t
    :config
    (add-to-list 'company-backends 'company-web-html)
    ;; (use-package ac-html-csswatcher
    ;;   :ensure t
    ;;   :config
    ;;   (setq ac-html-csswatcher-command "/usr/local/bin/csswatcher")
    ;;   (company-web-csswatcher-setup)
    ;;   )
    ;; )
    )
  )

(use-package sass-mode :ensure t)

(use-package markdown-mode :ensure t)

(use-package json-mode :ensure t)

(use-package magit
  :ensure t
  :config (global-set-key (kbd "C-x g") 'magit-status)
  )

(use-package sr-speedbar
  :ensure t
  :config
  (global-set-key (kbd "C-c s") 'sr-speedbar-toggle)
  (setq sr-speedbar-max-width 10)
  (speedbar-add-supported-extension ".sass")
  (speedbar-add-supported-extension ".ts")
  )

(use-package tide
  :ensure t
  :after (typescript-mode company flycheck)
  :config
  (setq tide-always-show-documentation t)
  (setq tide-completion-ignore-case t)
  (setq tide-completion-detailed t)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
	 (typescript-mode . electric-pair-local-mode)
         (before-save . tide-format-before-save))
	)

(column-number-mode +1)

(menu-bar-mode -1)

(xterm-mouse-mode +1)
(global-set-key [mouse-4] 'scroll-down-line)
(global-set-key [mouse-5] 'scroll-up-line)

(provide 'init-ts)
;;; init-ts.el ends here
