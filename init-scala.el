;;; .emacs -- My cool Emacs configuration for Scala development

;;; Commentary:

;;; Code:
;;; FlyCheck wants me to add this section.
;;; But this is actually...

;;; Generated stuff:
;;; Things that are added automagically, meaning not by me.
;;; ...but the next directives actually are.  Just nothing after that.
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (ensime markdown-mode company sr-speedbar git-commit magit flycheck use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(condition-case nil
    (require 'use-package)
  (file-error
   (package-refresh-contents)
   (package-install 'use-package)
   (require 'use-package)
   )
  )

;; use-package calls
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode)
  )

(use-package company
  :ensure t
  :config
  (global-company-mode)
  (setq company-tooltip-limit 15)
  (setq company-idle-delay 0)
  (setq company-echo-delay 0)
  (setq company-minimum-prefix-length 2)
  (setq company-require-match nil)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)
  )

(use-package markdown-mode :ensure t)

(use-package magit
  :ensure t
  :config (global-set-key (kbd "C-x g") 'magit-status)
  )

(use-package sr-speedbar
  :ensure t
  :config
  (global-set-key (kbd "C-c s") 'sr-speedbar-toggle)
  (setq sr-speedbar-max-width 10)
  (speedbar-add-supported-extension ".scala")
  (speedbar-add-supported-extension ".sbt")
  )

(use-package ensime
  :ensure t
  :config
  (setq ensime-startup-notification nil)
  (setq ensime-eldoc-hints 'all)
  )

(column-number-mode +1)

(menu-bar-mode -1)

(xterm-mouse-mode +1)
(global-set-key [mouse-4] 'scroll-down-line)
(global-set-key [mouse-5] 'scroll-up-line)

(provide 'init-scala)
;;; init-scala.el ends here
