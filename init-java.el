;;; .emacs -- My cool minimal Emacs configuration

;;; Commentary:
;; This init file is to be used as a template for creating other init files.
;; It includes the packages I *always* use.

;;; Code:
;;; FlyCheck wants me to add this section.
;;; But this is actually...

;;; Generated stuff:
;;; Things that are added automagically, meaning not by me.
;;; ...but the next directives actually are.  Just nothing after that.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (lsp-java dap-mode lsp-ui company-lsp lsp-mode flycheck sr-speedbar magit use-package company))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(package-initialize)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(require 'cc-mode)

(condition-case nil
    (require 'use-package)
  (file-error
   (package-refresh-contents)
   (package-install 'use-package)
   (require 'use-package)
   )
  )

(use-package lsp-mode
  :ensure t
  :init (setq lsp-eldoc-render-all nil))

(use-package company-lsp
  :after  company
  :ensure t
  :config
  (add-hook 'java-mode-hook
	    (lambda()
	      (push 'company-lsp company-backends)))
  (setq company-lsp-cache-candidates t)
  )

(use-package company
  :ensure t
  :config
  (global-company-mode)
  (setq company-tooltip-limit 15)
  (setq company-idle-delay 0)
  (setq company-echo-delay 0)
  (setq company-minimum-prefix-length 2)
  (setq company-require-match nil)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)

  (add-hook 'eshell-mode-hook '(lambda () (setq-local company-idle-delay nil)))
  )

(use-package lsp-ui
  :ensure t
  :config (setq lsp-ui-sideline-update-mode 'point))

(use-package dap-mode
  :ensure t
  :after  lsp-mode
  :config
  (dap-mode 1)
  (dap-ui-mode 1))

(use-package dap-java
  :after 'lsp-java)

(use-package lsp-java
  :ensure t
  :config
  (setq lsp-java-server-install-dir "/home/emacs/eclipsejdtls")
  (push "-javaagent:/usr/home/emacs/.m2/repository/org/projectlombok/lombok/1.16.22/lombok-1.16.22.jar" lsp-java-vmargs)
  (push "-Xbootclasspath/a:/usr/home/emacs/.m2/repository/org/projectlombok/lombok/1.16.22/lombok-1.16.22.jar" lsp-java-vmargs)
  ; (setq lsp-java-vmargs "-javaagent:/usr/home/emacs/.m2/repository/org/projectlombok/lombok/1.16.22/lombok-1.16.22.jar -Xbootclasspath/a:/usr/home/emacs/.m2/repository/org/projectlombok/lombok/1.16.22/lombok-1.16.22.jar")
  (add-hook 'java-mode-hook 'lsp-java-enable)
  (add-hook 'java-mode-hook 'lsp-ui-mode)
  (add-hook 'java-mode-hook 'electric-pair-local-mode)
  )

(use-package flycheck
  :ensure t
  :config (global-flycheck-mode)
  )

(use-package magit
  :ensure t
  :init (global-set-key (kbd "C-x g") 'magit-status)
  )

(use-package sr-speedbar
  :ensure t
  :config
  (global-set-key (kbd "C-c s") 'sr-speedbar-toggle)
  (speedbar-add-supported-extension ".cs")
  (speedbar-add-supported-extension ".csproj")
  (speedbar-add-supported-extension ".sln")
  (speedbar-add-supported-extension ".json")
  (speedbar-add-ignored-directory-regexp "[/\\]bin?[/\\]\\'")
  (setq sr-speedbar-max-width 10)
  (setq sr-speedbar-auto-refresh nil)
  )

;; Turn this on if you need it
;(require 'desktop)
;(desktop-save-mode 1)
;(setq desktop-auto-save-timeout 300)

(setq completion-ignore-case t)

(menu-bar-mode -1)
(column-number-mode +1)

;; Mouse settings
(global-set-key [mouse-5] 'scroll-up-line)
(global-set-key [mouse-4] 'scroll-down-line)
(xterm-mouse-mode +1)

(provide '.init-csharp)
;;; init-csharp.el ends here
