;;; .emacs -- My cool Emacs configuration for .NET development

;;; Commentary:
;; This init file should provide a usable .NET development environment,
;; with a little focus on portable .NET Core applications.

;;; Code:
;;; FlyCheck wants me to add this section.
;;; But this is actually...

;;; Generated stuff:
;;; Things that are added automagically, meaning not by me.
;;; ...but the next directives actually are.  Just nothing after that.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (json-mode sr-speedbar magit use-package omnisharp dotnet company))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(package-initialize)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(condition-case nil
    (require 'use-package)
  (file-error
   (package-refresh-contents)
   (package-install 'use-package)
   (require 'use-package)
   )
  )

(use-package omnisharp
  :ensure t
  :init
  (add-hook 'csharp-mode-hook
	    '(lambda()
	       (omnisharp-mode)

	       (setq indent-tabs-mode nil)
	       (setq c-syntactic-indentation t)
	       (c-set-style "ellemtel")
	       (c-set-offset 'arglist-intro '+)
	       (c-set-offset 'arglist-cont-nonempty '+)
	       (setq c-basic-offset 4)
	       (setq truncate-lines t)
	       (setq tab-width 4)
	       (setq evil-shift-width 4)

	       (electric-pair-local-mode 1)

	       (local-set-key (kbd "C-c r r") 'omnisharp-run-code-action-refactoring)
	       (local-set-key (kbd "C-c C-c") 'recompile)
	       )
	    )
  (setq omnisharp-company-ignore-case t)
  )

(use-package json-mode :ensure t)

(use-package tide
  :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(use-package dotnet
  :ensure t
  :config (add-hook 'csharp-mode-hook 'dotnet-mode)
  )

(use-package company
  :ensure t
  :config
  (global-company-mode)
  (setq company-tooltip-limit 15)
  (setq company-idle-delay 0)
  (setq company-echo-delay 0)
  (setq company-minimum-prefix-length 2)
  (setq company-require-match nil)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)
  (add-to-list 'company-backends #'company-omnisharp)
  (add-hook 'eshell-mode-hook '(lambda () (setq-local company-idle-delay nil)))
  )

(use-package flycheck
  :ensure t
  :config (global-flycheck-mode)
  )

(use-package magit
  :ensure t
  :init (global-set-key (kbd "C-x g") 'magit-status)
  )

(use-package sr-speedbar
  :ensure t
  :config
  (global-set-key (kbd "C-c s") 'sr-speedbar-toggle)
  (speedbar-add-supported-extension ".cs")
  (speedbar-add-supported-extension ".csproj")
  (speedbar-add-supported-extension ".sln")
  (speedbar-add-supported-extension ".json")
  (speedbar-add-supported-extension ".ts")
  (speedbar-add-ignored-directory-regexp "[/\\]bin?[/\\]\\'")
  (setq sr-speedbar-max-width 10)
  (setq sr-speedbar-auto-refresh nil)
  )

(require 'desktop)
(desktop-save-mode 1)
(setq desktop-auto-save-timeout 300)

(setq completion-ignore-case t)

(menu-bar-mode -1)
(column-number-mode +1)

(add-hook 'after-init-hook
	  '(lambda()
	     (interactive)
	     (split-window-below)
	     (other-window 1)
	     (while (> (window-body-height) 10)
	       (shrink-window 1))
	     (eshell)
	     (other-window 1)
	     ))

(when (window-system) (set-face-attribute 'default nil :height 100))

;; Mouse settings
(global-set-key [mouse-5] 'scroll-up-line)
(global-set-key [mouse-4] 'scroll-down-line)
(xterm-mouse-mode +1)

(load-theme 'tango-dark t)

(provide '.init-csharp)
;;; init-csharp.el ends here
