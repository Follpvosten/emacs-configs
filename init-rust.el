;;; .emacs -- My Emacs configuration for Rust

;;; Commentary:
;; This init file is supposed to turn Emacs into a Rust development environment.
;; It has stuff like toml-mode and rust-mode and *should* feature some kind of
;; working auto-completion in any case.

;;; Code:
;;; FlyCheck wants me to add this section.
;;; But this is actually...

;;; Generated stuff:
;;; Things that are added automagically, meaning not by me.
;;; ...but the next directives actually are.  Just nothing after that.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (spinner lsp-rust company-lsp lsp-ui-flycheck lsp-ui lsp-mode toml-mode cargo racer flycheck-rust rust-mode flycheck sr-speedbar magit use-package company))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(package-initialize)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(condition-case nil
    (require 'use-package)
  (file-error
   (package-refresh-contents)
   (package-install 'use-package)
   (require 'use-package)
   )
  )

(use-package rust-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
  (setq rust-format-on-save t)
  )

(use-package flycheck
  :ensure t
  :config (global-flycheck-mode)
  )

;; (use-package flycheck-rust
;;   :ensure t
;;   :config
;;   (with-eval-after-load 'rust-mode
;;     (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
;;   )

;; (use-package racer
;;   :ensure t
;;   :config
;;   (add-hook 'rust-mode-hook #'racer-mode)
;;   (add-hook 'racer-mode-hook #'eldoc-mode)
;;   (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
;;   )

(use-package lsp-mode
  :ensure t
  :commands lsp
  :init
  (setq lsp-prefer-flymake nil)
  (setq lsp-enable-snippet nil)
  (add-hook 'rust-mode-hook #'lsp)
  )

;; Does not work and isn't needed atm - might be re-added later.
;(use-package lsp-rust
;  :ensure t
;  )

(use-package lsp-ui
  :ensure t
  :commands lsp-ui
  )

(use-package company-lsp
  :ensure t
  :commands company-lsp
  )

(use-package toml-mode :ensure t)

(use-package cargo
  :ensure t
  :init
  (add-hook 'rust-mode-hook 'cargo-minor-mode )
  (add-hook 'toml-mode-hook 'cargo-minor-mode )
  )

(use-package company
  :ensure t
  :config
  (global-company-mode)
  (setq company-tooltip-limit 15)
  (setq company-idle-delay 0)
  (setq company-echo-delay 0)
  (setq company-minimum-prefix-length 2)
  (setq company-require-match nil)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)

  (add-hook 'eshell-mode-hook '(lambda () (setq-local company-idle-delay nil)))
  )

(use-package magit
  :ensure t
  :init (global-set-key (kbd "C-x g") 'magit-status)
  )

(use-package sr-speedbar
  :ensure t
  :config
  (global-set-key (kbd "C-c s") 'sr-speedbar-toggle)
  (speedbar-add-supported-extension ".rs")
  (speedbar-add-supported-extension ".toml")
  (setq sr-speedbar-max-width 10)
  (setq sr-speedbar-auto-refresh nil)
  )

;; Turn this on if you need it
;(require 'desktop)
;(desktop-save-mode 1)
;(setq desktop-auto-save-timeout 300)

(setq completion-ignore-case t)

(menu-bar-mode -1)
(column-number-mode +1)

;; Mouse settings
(global-set-key [mouse-5] 'scroll-up-line)
(global-set-key [mouse-4] 'scroll-down-line)
(xterm-mouse-mode +1)

(load-theme 'tango-dark t)

(provide '.init-rust)
;;; init-rust.el ends here
